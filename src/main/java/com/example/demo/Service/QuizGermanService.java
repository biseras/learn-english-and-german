package com.example.demo.Service;

import com.example.demo.Model.QuestionFormGerman;
import com.example.demo.Model.QuestionGerman;

import java.util.List;
import java.util.Optional;

public interface QuizGermanService {
    QuestionFormGerman getQuestionsForQuiz();

    List<QuestionGerman> findAll();

    Optional<QuestionGerman> add(String title, String optionA, String optionB, String optionC, Integer answer);

    int getResult(QuestionFormGerman questionForm);
}