package com.example.demo.Service.Implementation;

import com.example.demo.Model.Question;
import com.example.demo.Model.QuestionForm;
import com.example.demo.Model.QuestionFormGerman;
import com.example.demo.Model.QuestionGerman;
import com.example.demo.Repository.QuestionGermanRepository;
import com.example.demo.Repository.QuestionRepository;
import com.example.demo.Service.QuizGermanService;
import com.example.demo.Service.QuizService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@AllArgsConstructor
public class QuizServiceGermanImpl implements QuizGermanService {
    private final QuestionGermanRepository questionRepository;

    @Autowired
    QuestionFormGerman questionForm;

    @Override
    public QuestionFormGerman getQuestionsForQuiz() {
        List<QuestionGerman> questions = this.questionRepository.findAll();
        List<QuestionGerman> listWith3Questions = new ArrayList<>();
        Random random = new Random();
        for(int i = 0; i < 6; i++){
            Integer number = random.nextInt(questions.size());
            listWith3Questions.add(questions.get(number));
            questions.remove(questions.get(number));
        }
        this.questionForm.setQuestions(listWith3Questions);
        return questionForm;
    }

    @Override
    public List<QuestionGerman> findAll() {
        return this.questionRepository.findAll();
    }

    @Override
    public Optional<QuestionGerman> add(String title, String optionA, String optionB, String optionC, Integer answer) {
        return Optional.of(this.questionRepository.save(new QuestionGerman(title, optionA, optionB, optionC, answer)));
    }

    @Override
    public int getResult(QuestionFormGerman questionForm) {
        int correct = 0;

        for(QuestionGerman p : questionForm.getQuestions())
        {
            if(p.getAns() == p.getChose())
                correct++;
        }
        return correct;
    }
}