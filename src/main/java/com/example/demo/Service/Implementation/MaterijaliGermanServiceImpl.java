package com.example.demo.Service.Implementation;

import com.example.demo.Model.Materijali;
import com.example.demo.Model.MaterijaliGerman;
import com.example.demo.Model.exception.PasswordsDoNotMatchException;
import com.example.demo.Repository.MaterijaliGermanRepository;
import com.example.demo.Repository.MaterijaliRepository;
import com.example.demo.Service.MaterijaliGermanService;
import com.example.demo.Service.MaterijaliService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MaterijaliGermanServiceImpl implements MaterijaliGermanService {
    private final MaterijaliGermanRepository materijaliRepository;

    public MaterijaliGermanServiceImpl(MaterijaliGermanRepository materijaliRepository) {
        this.materijaliRepository = materijaliRepository;
    }

    @Override
    public Optional<MaterijaliGerman> findById(Long id) {
        return materijaliRepository.findById(id);
    }

    @Override
    public List<MaterijaliGerman> listAll() {
        return materijaliRepository.findAll();
    }

    @Override
    public Optional<MaterijaliGerman> save(String naslov, String opis, String link) {
        MaterijaliGerman materijali=new MaterijaliGerman(naslov, opis, link);
        return Optional.of(this.materijaliRepository.save(materijali));
    }

    @Override
    public Optional<MaterijaliGerman> edit(Long id, String naslov, String opis, String link) {
        MaterijaliGerman materijali=materijaliRepository.findById(id).orElseThrow(()->new PasswordsDoNotMatchException());
        materijali.setNaslov(naslov);
        materijali.setOpis(opis);
        materijali.setLink(link);
        return Optional.of(this.materijaliRepository.save(materijali));
    }

    @Override
    public void deleteById(Long id) {
        materijaliRepository.deleteById(id);
    }
}

