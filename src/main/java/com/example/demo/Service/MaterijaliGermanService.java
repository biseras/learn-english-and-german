package com.example.demo.Service;

import com.example.demo.Model.Materijali;
import com.example.demo.Model.MaterijaliGerman;

import java.util.List;
import java.util.Optional;

public interface MaterijaliGermanService {
    Optional<MaterijaliGerman> findById(Long id);
    List<MaterijaliGerman> listAll();

    Optional<MaterijaliGerman> save(String naslov, String opis, String link);
    Optional<MaterijaliGerman> edit(Long id, String naslov, String opis, String link);


    void deleteById(Long id);
}
