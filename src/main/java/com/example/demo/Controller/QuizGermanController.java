package com.example.demo.Controller;

import com.example.demo.Model.Question;
import com.example.demo.Model.QuestionForm;
import com.example.demo.Model.QuestionFormGerman;
import com.example.demo.Model.QuestionGerman;
import com.example.demo.Service.QuizGermanService;
import com.example.demo.Service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/kvizgerman")
public class QuizGermanController {

    private final QuizGermanService quizService;

    public QuizGermanController(QuizGermanService quizService) {
        this.quizService = quizService;
    }

    @Autowired
    QuestionFormGerman questionForm;

    @ModelAttribute("quiz")
    public QuestionFormGerman getQuestionForm(){
        return questionForm;
    }

    @GetMapping
    public String kviz(Model model){
        QuestionFormGerman questionForm1 = quizService.getQuestionsForQuiz();
        model.addAttribute("bodyContent", "kvizgerman");
        model.addAttribute("qForm", questionForm1);
        return "master-template";
    }

    @GetMapping("/dodadi-prasanje")
    public String dodadiPrasanjeForma(Model model){
        model.addAttribute("bodyContent", "dodadiprasanjegerman");
        return "master-template";
    }

    @PostMapping("/dodadi-prasanje")
    public String dodadiPrasanje(@RequestParam String title,
                                 @RequestParam String optionA,
                                 @RequestParam String optionB,
                                 @RequestParam String optionC,
                                 @RequestParam Integer ans){
        this.quizService.add(title, optionA, optionB, optionC, ans);
        return "redirect:/kvizgerman/prasanja";
    }

    @GetMapping("/prasanja")
    public String sitePrasanja(Model model){
        List<QuestionGerman> questions = this.quizService.findAll();
        model.addAttribute("bodyContent", "siteprasanjagerman");
        model.addAttribute("prasanja", questions);
        return "master-template";
    }

    @GetMapping("/rezultati")
    public String rezultati(@RequestParam(required = false) String error, @RequestParam(required = false) String poeni, Model model){
        if(error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }
        if(poeni != null && !poeni.isEmpty()) {
            model.addAttribute("zavrseno", true);
            model.addAttribute("poeni", poeni);
        }
        model.addAttribute("bodyContent", "rezultatigerman");
        return "master-template";
    }

    @PostMapping
    public String resiKviz(@ModelAttribute QuestionFormGerman questionForm, Model model){
        int poeni = quizService.getResult(questionForm);
        model.addAttribute("poeni", poeni);
        return "redirect:/kvizgerman/rezultati?poeni=Du hast "+poeni+" punkte";
    }
}