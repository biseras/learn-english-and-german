package com.example.demo.Model;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class QuestionFormGerman {

    private List<QuestionGerman> questions;

    public List<QuestionGerman> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionGerman> questions) {
        this.questions = questions;
    }
}