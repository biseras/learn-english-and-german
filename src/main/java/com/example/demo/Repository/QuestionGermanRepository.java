package com.example.demo.Repository;

import com.example.demo.Model.Question;
import com.example.demo.Model.QuestionGerman;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionGermanRepository extends JpaRepository<QuestionGerman, Integer> {

}