package com.example.demo.Repository;

import com.example.demo.Model.Materijali;
import com.example.demo.Model.MaterijaliGerman;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterijaliGermanRepository extends JpaRepository<MaterijaliGerman, Long> {
}